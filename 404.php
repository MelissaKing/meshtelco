<?php /**
 * The template for displaying 404 pages (not found)
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */?>

<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>
	<?php include 'partials/header.php';?>

	<!-- Document Title
	============================================= -->
	<title><?php bloginfo( 'name' ); ?> |  Oh no! 404!</title>

</head>

<body class="stretched no-transition fourofour">
	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TH927RC"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->
	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">

		<?php include 'partials/menu.php';?>
        

		<!-- Content
		============================================= -->
		<section id="page-title" class="page-title-parallax full-screen dark error404-wrap" style="background: url(<?php echo esc_url( get_template_directory_uri() ); ?>/images/hero-404.png) center;" data-stellar-background-ratio="0.5">
			<div class="container vertical-middle center clearfix">
				<div class="error404">404</div>
				<div class="heading-block nobottomborder">
					<h4>Hey, this doesn't look right...</h4>
					<span>Did you take a wrong turn? Return to <a href="javascript:history.back()">where you were</a> or <a href="/">return home</a>.</span>
				</div>
			</div>
		</section>

		<?php include 'partials/footer.php';?>

	</div><!-- #wrapper end -->
	<!-- Footer Scripts
	============================================= -->
	<script type="text/javascript" src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/functions.js"></script>

</body>
</html>