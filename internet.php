<?php /**
 * Template Name: Internet
 *
 * @package WordPress
 */
$pageID = get_the_ID();
//Standard field includes for SEO, Hero, and intro
include 'partials/fields.php';
?>

<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>
	<?php include 'partials/header.php';?>
</head>

<body class="stretched <?php echo $page_class; ?>" data-loader-color="#E30160">
	<div class="loader-holder">
		<img class="loader" src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/loader.gif"/>
	</div>
	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TH927RC"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->
	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix" style="display:none;">

		<?php include 'partials/menu.php';
		//checks whether the featured image is set and then sets it to the hero
		if (has_post_thumbnail($pageID) ):
		$image = wp_get_attachment_image_src( get_post_thumbnail_id( $pageID ), 'single-post-thumbnail' );
		endif; ?>
		
       	<!-- Hero image and page title
        ============================================= -->
        <?php include 'partials/pagetitle.php';?>

		<!-- Content
		============================================= -->
        <section id="content">
			<div class="content-wrap">
				<?php 
				if(!(strstr($page_class,'plans'))){
					include 'partials/breadcrumb.php';
				}
				include 'partials/intro.php';

				//include pagelinks if top level page
				if(!($post->post_parent = 0)){
					include 'partials/pagelinks.php';
				}
				//checks if page ID is equal to Embedded Networks page
				if(in_array($pageID,array(374))){
					include 'partials/testimonials.php';
				//Page ID for business landing page
				}else if(in_array($pageID,array(338))){
					include 'partials/testimonials.php';
					include 'partials/planpreviewer.php';
				}else{
					include 'partials/planpreviewer.php';
				}?>
				<section id="contact" class="section cta nomargin nobottomborder">
					<div class="container clearfix nobottomborder">
					   <div class="col_one_fourth">
						</div>
						<div class="column col_half col_last nobottommargin center">
							<?php include 'partials/cta.php';?>
						</div>
						<div class="col_one_fourth col_last">
						</div>
					</div>
				</section>
			</div>
		</section><!-- #content end -->


		<?php include 'partials/footer.php';?>

	</div><!-- #wrapper end -->
</body>
</html>
