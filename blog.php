<?php /**
 * Template Name: Blog
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */	
$pageID = get_the_ID();
$meta_description = get_field("seo_meta_description", $pageID);
$meta_title = get_field("seo_meta_title", $pageID);
$page_title = get_field("page_title", $pageID );
?>
<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>
	<?php include 'header.php';?>

	<!-- Document Title
	============================================= -->
	<meta name="description" content="<?php echo $meta_description;?>"/>
	<title><?php bloginfo( 'name' ); ?> | <?php echo $meta_title;?></title>

</head>

<body class="stretched blog" data-loader-color="#E30160">
	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TH927RC"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->
	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">

		<?php include 'menu.php';?>
        
        <section id="slider" class="slider-parallax hero-blog">
            
			<div class="container clearfix">
                <div class="heading-block nobottomborder col_half">
                    
					<h1>
						<?php echo $page_title;?>
					</h1>
                </div>
            </div>
		</section>
        

		<!-- Content
		============================================= -->
		<section id="content">
			<div class="nopadding content-wrap">
           
            
            <!-- intro -->
            <div class="section nopadding nomargin">
                    <div class="container clearfix nobottommargin topmargin-lg">
                        <div class="row clearfix nomargin">
                        	<div class="col_full">
                            	<!-- Posts
						============================================= -->
								<div id="posts" class="col_three_fourth">
                        


                                    <!-- Post Content
                                    ============================================= -->
                                    <div class="postcontent nobottommargin clearfix">
                                        
            
                                        <?php
											$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
											$args = array(
												'posts_per_page'	=>  5,
												'orderby'           => 'post_date',
												'order'             => 'DESC',
												'post_type'         => 'post',
												'post_status'       => 'publish',
												'paged' => $paged
											);
											
											$blogPosts = new WP_Query( $args );
											
		
										 if ( $blogPosts-> have_posts() ) : 
										
                                        // Start the loop.
                                        while ($blogPosts-> have_posts() ) : $blogPosts->the_post();
                            
                                            /*
                                             * Include the Post-Format-specific template for the content.
                                             * If you want to override this in a child theme, then include a file
                                             * called content-___.php (where ___ is the Post Format name) and that will be used instead.
                                             */
                                            get_template_part( 'content_preview', get_post_format() );
                            
                                        // End the loop.
                                        endwhile;
                            
                                        
                                        // If no content, include the "No posts found" template.
                                        else :
                                            get_template_part( 'content_preview', 'none' );
                                
                                        endif;
                                        ?>
                                        
                                        <!-- Pagination
                                    ============================================= -->
                                    
                                    
                                    
                                    
                                    <div class="clear"></div>
                                    
                                    <?php if ($blogPosts->max_num_pages > 1) { // check if the max number of pages is greater than 1  ?>
                                      <nav class="prev-next-posts">
                                        <div class="prev-posts-link">
                                          <?php echo get_next_posts_link( 'Older Entries', $the_query->max_num_pages ); // display older posts link ?>
                                        </div>
                                        <div class="next-posts-link">
                                          <?php echo get_previous_posts_link( 'Newer Entries' ); // display newer posts link ?>
                                        </div>
                                      </nav>
                                    <?php } ?>
                                    
                                    
                                    
                                    
                                    
                                        <div class="pager margintop-lg">
                                        <?php // Previous/next page navigation.
                                        the_posts_pagination( array(
                                            'prev_text'          => __( '&laquo;', 'Draftee' ),
                                            'next_text'          => __( ' &raquo;', 'Draftee' ),
                                            'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( '', 'twentyfifteen' ) . ' </span>',
                                        ) );?>
                                        </div><!-- .pager end -->
                						 <?php wp_reset_postdata(); ?>
                                    </div><!-- .postcontent end -->
                                </div>
                                <?php include 'right_sidebar.php';?>
                            </div>
                        </div>
                    </div>
                </div>
                
            

                 <?php include 'cta.php';?>
    
		</section><!-- #content end -->

		<?php include 'footer.php';?>

	</div><!-- #wrapper end -->
	<!-- Footer Scripts
	============================================= -->
	<script type="text/javascript" src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/functions.js"></script>

</body>
</html>