<?php /**
 * Template Name: Legal
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */	
$pageID = get_the_ID();
$staff_profiles =  get_field("staff_profiles", $pageID );
//Standard field includes for SEO, Hero, and intro
include 'partials/fields.php';
?>

<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>
	<?php include 'partials/header.php';?>
</head>

<body class="stretched <?php echo $page_class; ?>" data-loader-color="#E30160">
	<div class="loader-holder">
		<img class="loader" src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/loader.gif"/>
	</div>
	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TH927RC"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->
	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">
        
        <?php 
		
		include 'partials/menu.php';
		//checks whether the featured image is set and then sets it to the hero
		if (has_post_thumbnail($pageID) ): 
		$image = wp_get_attachment_image_src( get_post_thumbnail_id( $pageID ), 'single-post-thumbnail' ); 
		endif; ?>
        
        <!-- Hero image and page title
        ============================================= -->
        <?php include 'partials/pagetitle.php';?>

		<!-- Content
		============================================= -->
		<section id="content">
			<div class="content-wrap">

            	<?php 
				
				include 'partials/breadcrumb.php';
				include 'partials/intro.php';
				
				?>

           		<section id="staff" class="section nomargin nobottomborder <?php if(!(strstr($page_class,'legal'))){ echo $page_class;?>center<?php } ?>">
           			<div class="container clearfix nobottomborder">
           				<?php the_content();?>
					</div>
				</section>
            	
				<section id="contact" class="section cta nomargin nobottomborder">
					<div class="container clearfix nobottomborder">
					   <div class="col_one_fourth">
						</div>
							<div class="column col_half col_last nobottommargin center">
								<?php include 'partials/cta.php';?>
							</div>
						<div class="col_one_fourth col_last">
						</div>
					</div>
				</section>
            </div>
    
		</section><!-- #content end -->

		<?php include 'partials/footer.php';?>

	</div><!-- #wrapper end -->
	<!-- Footer Scripts
	============================================= -->
	<script type="text/javascript" src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/functions.js"></script>

</body>
</html>