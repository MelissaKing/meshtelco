<?php

function lifewireless_scripts() {
    wp_dequeue_script( 'jquery' );
    wp_enqueue_script('lifewireless_scripts', get_template_directory_uri() . '/js/meshtelco/scripts.js', array(), null, true);
}
add_action( 'wp_enqueue_scripts', 'lifewireless_scripts' );

add_action('wp_ajax_lifewireless_plan_request', array('Lifewireless_Post', 'lifewireless_plan_request') );
add_action('wp_ajax_nopriv_lifewireless_plan_request', array('Lifewireless_Post', 'lifewireless_plan_request') );
add_action('wp_ajax_lifewireless_enquiry', array('Lifewireless_Post', 'lifewireless_enquiry') );
add_action('wp_ajax_nopriv_lifewireless_enquiry', array('Lifewireless_Post', 'lifewireless_enquiry') );
add_filter('wp_mail_content_type', array('Lifewireless_Post', 'lifewireless_mail_content_type') );

class Lifewireless_Post {
    const WEBHOOK_URL = 'https://hooks.slack.com/services/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';
    const SLACK_CHANNEL = '#lifewireless-leads';
    const SLACK_BOT = 'web_enquiry_bob';

    public static function lifewireless_enquiry() {
        if (isset($_POST['url'][0]) && isset($_POST['name'][0]) && isset($_POST['phone'][0]) && isset($_POST['email'][0])) {
            $payload_fallback = sprintf("Life Wireless General enquiry\n Name: %s\n Company: %s\n Phone: %s\n Email: %s\n Best time to call: %s\n Comments: %s\n URL: %s\n ", $_POST['name'], $_POST['business'], $_POST['phone'], $_POST['email'], $_POST['time'], $_POST['comments'], $_POST['url']);

            $data = [
                'Date' => date('d/m/Y h:ia e'),
                'Name' => $_POST['name'],
                'Phone' => $_POST['phone'],
                'Email' => $_POST['email'],
                'Time' => $_POST['time']
            ];

            if (isset($_POST['suburb'])) {
                $payload_fallback .= sprintf("Suburb: %s \n", $_POST['suburb']);
                $data['Suburb'] = $_POST['suburb'];
            }
			
			if (isset($_POST['address'])) {
                $payload_fallback .= sprintf("Address: %s \n", $_POST['address']);
                $data['Address'] = $_POST['address'];
            }
			
            if (isset($_POST['business'])) {
                $payload_fallback .= sprintf("Business: %s \n", $_POST['business']);
                $data['Business'] = $_POST['business'];
            }
			
			if (isset($_POST['meshproduct']) && is_array($_POST['meshproduct'])) {
			   $payload_fallback .= sprintf("Products: %s \n", implode(', ', $_POST['meshproduct']));
			   $data['Products'] = implode(', ', $_POST['meshproduct']);
			}

            if (isset($_POST['comments'])) {
                $payload_fallback .= sprintf("Comments: %s \n", $_POST['comments']);
                $data['Comments'] = $_POST['comments'];
            }

            $data['URL'] = $_POST['url'];
            $subject = 'Life Wireless General enquiry';

            self::send_slack_request($subject, $data, $payload_fallback);
            $mail = self::send_email($subject, $data);

            if ($mail) {
                echo 'success';
            } else {
                status_header( 400 );
            }

        } else {
            status_header( 400 );
        }
        exit();
    }

    public static function lifewireless_plan_request() {

        if (isset($_POST['planid']) && isset($_POST['name'][0]) && isset($_POST['suburb'][0]) && isset($_POST['phone'][0]) && isset($_POST['email'][0]) && isset($_POST['termchosen'][0])) {

            $plan = get_post((int)$_POST['planid']);
            if (!$plan)
                return status_header(400);

            $parent_id = wp_get_post_parent_id( $plan->ID );
            if (!$parent_id)
                return status_header(400);
            $connection = get_post($parent_id);
            $plan_name = sprintf('%s - %s', $connection->post_title, $plan->post_title);

            $data = [
                'Date' => date('d/m/Y h:ia e'),
                'Name' => $_POST['name'],
                'Phone' => $_POST['phone'],
                'Email' => $_POST['email'],
                'Suburb' => $_POST['suburb'],
                'Plan' => $plan_name,
                'Term' => $_POST['termchosen'] . ' month',
                'Time' => $_POST['time'],
            ];

            $payload_fallback = sprintf("Life Wireless Internet enquiry - %s\n Name: %s\n Phone: %s\n Email: %s\n Suburb: %s\n Best time to call: %s", $plan_name, $_POST['name'], $_POST['phone'], $_POST['email'], $_POST['suburb'], $_POST['time']);

            if (isset($_POST['business'])) {
                $payload_fallback .= sprintf("Business: %s \n", $_POST['business']);
                $data['Business'] = $_POST['business'];
            }
			
			if (isset($_POST['meshproduct']) && is_array($_POST['meshproduct'])) {
			   $payload_fallback .= sprintf("Products: %s \n", implode(', ', $_POST['meshproduct']));
			   $data['Products'] = implode(', ', $_POST['meshproduct']);
			}
			


            $subject = "Internet enquiry - " . $plan_name;
            self::send_slack_request($subject, $data, $payload_fallback);
            $mail = self::send_email($subject, $data);

            if ($mail) {
                echo 'success';
            } else {
                status_header( 400 );
            }
        }
        else {
            status_header( 400 );
        }
        exit();

    }
    public static function send_email($subject, $data) {

        $to = get_option('admin_email');
        $headers = 'From: "LifeWireless Website" <info@lifewireless.com.au>';

        ob_start();
        foreach ($data as $k => $v) {
            printf('<b>%s</b><br />%s', $k, wpautop($v));
        }

        $message = ob_get_contents();

        ob_end_clean();

        return wp_mail($to, $subject, $message, $headers);
    }

    public static function send_slack_request($subject, $data, $fallback = '') {
        $fields = [];
        foreach ($data as $k => $v) {
            $fields []= [
                "title" => $k,
                "value" => $v,
                "short" => true
            ];
        }
        $payload = [
            'channel' => self::SLACK_CHANNEL,
            'username' => self::SLACK_BOT,
            'attachments' => [
               [
                   'fallback' => $fallback,
                   "color" => "#4F2580",
                   "title" => $subject,
                   "fields" => $fields,
                   "ts" => time()
               ]
           ]
        ];
        try {
            wp_remote_post(self::WEBHOOK_URL, [ 'method' => 'POST', 'body' => [ 'payload' => json_encode($payload) ] ]);
        } catch (\Exception $e) {
        }

    }

    public static function lifewireless_mail_content_type() {
        return 'text/html';
    }
}
