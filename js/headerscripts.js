$(document).ready(function() {
	'use strict';
	var $window = $(window);
	$('#menu-top-menu').addClass('clearfix');
	$('<div></div>').appendTo('#primary-menu > div');

	 //Loggly Init
	 var _LTracker = _LTracker || [];
	_LTracker.push({'logglyKey': '186e6dea-5f62-43a0-9cd9-6da99275cdb0',
	'sendConsoleErrors' : false,
	'tag' : 'loggly-jslogger'  });
	
	 //Hamburger menu trigger, no plugins
	$('.menu-icon,.menu-top-menu-container .menu-item,#primary-menu > div > div').on('click',function(){
		if ($window.width() < 991) {
			$('#header-wrap').addClass("mobile");
			$('#menu-top-menu').fadeToggle('fast','linear');
			$('#primary-menu > div > div').toggleClass('open');
		}
	});
	
	$window.resize(function (){
		$('#primary-menu > div > div').removeClass('open');
		if ($window.width() > 977) {
			$('#header-wrap').removeClass("mobile");
			$('#menu-top-menu').fadeIn('fast','linear');
		}else{
			$('#header-wrap').addClass("mobile");
			$('#menu-top-menu').fadeOut('fast','linear');
		}
	});

	 //http://www.jqueryscript.net/slider/Responsive-Image-Carousel-jQuery-SimpleSlider.html
	if ($('.slider').length) {
	   $('.slider').freeSimpleSlider({
		   dots: true,
		   arrows: true,
		   animation: "fade",
		   time:5000
		});
	}
	
	 //http://www.jqueryscript.net/other/Themeable-jQuery-Tabs-Plugin-CardTabs.html
	if ($('.tabs').length) {
		$('.tabs').cardTabs();
	}

	 //http://www.jqueryscript.net/other/Responsive-Any-Element-Sticky-Plugin-For-jQuery-sticky-items.html
	if ($('.sticky').length) {
		$('.sticky').stickyItem();
	}

	 //http://www.jqueryscript.net/animation/Smooth-Scrolling-To-Anchor-Elements-jQuery-anchorLink.html
	 $('a.scroll').anchorlink({
	  timer : 500,
	  scrollOnLoad : true,
	  offsetTop : -100,
	  focusClass : 'js-focus',
	  beforeScroll: function() {},
	  afterScroll : function() {}
	});
	
	// Google Tag Manager
	(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!=='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-M82KHL3');
	
	//Facebook footer widget
	(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.9";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
});