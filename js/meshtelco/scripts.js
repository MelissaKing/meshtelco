jQuery(document).ready(function() {
    jQuery('#ctaform, #contactform').on('submit', function(e) {
        e.preventDefault();
        var form = jQuery(this);
        var data = form.serialize();
		var url = location.href;
		var careers = "careers";
		var faq = "faq";
		var nbn = "nbn";
        var button = jQuery('button', form);
        button.html('Sending...').attr('disabled', 'disabled');
		if(typeof ga !== 'undefined' ){
			ga('send', 'event', 'contact-form', 'submit', 'Contact form from ' +document.location.pathname);
		}
		//gaq_push (document.location.pathname + 'general-enquiry');
		if(typeof goog_report_conversion !== 'undefined' ){
			goog_report_conversion(location.href + 'general-enquiry');
		}
        jQuery.post('/general-enquiry', data + '&url=' + location.href, function( response ) {
			//NBN register page response
			if (url.toLowerCase().indexOf(nbn) === -1){
            	form.html('<div class="cta-response"><h4>Thank you for your registration!</h4><p>We will add you to our database and check on NBN installation progress for your area regularly to keep you updated. For any enquiries, please call us on 1300 080 820.</p></div>');
			//FAQ page response
			}else if (url.toLowerCase().indexOf(faq) === -1){
				form.html('<div class="cta-response"><h4>Thank you for your question!</h4><p>We will endeavour to answer your question as promptly as possible and update our FAQ. For any enquiries, please call us on 1300 080 820.</p></div>');
			//careers page response
			}else if (url.toLowerCase().indexOf(careers) === -1){
				form.html('<div class="cta-response"><h4>Thank you for your application!</h4><p>We will be in touch shortly to discuss potential opportunities. For any enquiries, please call us on 1300 080 820.</p></div>');
			}else{
				form.html('<div class="cta-response"><h4>Thank you for your enquiry!</h4><p>We will be in touch shortly to discuss your requirements. For more urgent matters, please call us on 1300 080 820.</p></div>');
			}
        })
		.fail(function(xhr) {
			form.html('<div class="cta-response errormsg alert"><h4 class="nobottommargin">Oh no!</h4><p>Sorry, an error occurred. An error log has been sent to our development team for review. If you do not hear from us in 48 hours, please call 1300 080 820.</p></div>');
			button.html('Send email').removeAttr('disabled');
			//console.log('A form was not sent due to the following error: ' + xhr.status + ' ' + xhr.statusText + '\n We were able to retrieve the following data: ' + data + '\n More details: ' +xhr.getAllResponseHeaders());
			
			_LTracker.push('A form on MeshTelco was not sent due to the following error: ' + xhr.status + ' ' + xhr.statusText + '\n We were able to retrieve the following data: ' + data + '\n More details: ' +xhr.getAllResponseHeaders());
		  })
		;
    });
});
