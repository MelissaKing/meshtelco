var planDetails, planColor, planSignupLoc,e;
$(document).ready(function(){
	'use strict';
	
	$('#planSignup .planSignup-inner').hide();
	$('#planChooser').fadeIn();
	
	
	//toggles whether the address is a business or not, and shows EFM and MBE tabs if so. If unchecked when EFM or MBE tab active, sets focus back to NBN tab.
    $('#form-business-address').click(function(){
        $('.businessplan').toggle(this.checked);
		$('#planSignup #signup-step1 .businessfield').toggle();
        if ( $('.businessplan').hasClass('ui-tabs-active')){
            $('.tab-nav li:first-child a').click();
        }
    });

    //the address check goes here. I've put in a check to see whether it's blank but will also need to validate address
    //$('#address-check').click(function(){
//        if( $('#address-field').val() ) {
//			$('#address-check-scroll').click();
//        	$('.tabs').css('opacity','1');
//			$('.tabs').css('height','auto');
//            $('.alert-success').removeClass('hidden');
//        }else{
//			$.notice({
//				text: "Please enter your address.",
//				type: "warning",
//				position: 'bottom-right'
//
//			}); 
//        }
//    });
	
	
	
    //Proceeds to modem selection as soon as a plan button is pressed, store which plan at this point
    $(document).on('click','.plan-select',function(e){
		//console.log('yay')
		$('#planSignup .planSignup-inner').show();
		var termChosen = $(this).parent().siblings('.pricing-features').children('ul').children('.termSelector').children('#term').val();
        var planDetails = $(this).parent().parent().html();
		var wireChosen = $('#wire option:selected').text();
        var planColor = $(this).parent().parent().attr("class");
		//var termChosen = $(this).parent().siblings('.pricing-features').children('.termSelector .term').val();

        $('#planSignup .pricing-box').html(planDetails);

		$('#planSignup .pricing-box .pricing-features ul .termSelector').empty();
		
		//alert(termChosen);
		$('#planSignup .pricing-box .pricing-features ul .termSelector').html(termChosen + ' month term');
		$('#planSignup .pricing-box .pricing-features ul .wireSelector').html(wireChosen);
        $('#planSignup .pricing-box .pricing-action').remove();
        $('#planSignup .pricing-box').removeClass("homeplans businessplans");
        $('#planSignup .pricing-box').addClass(planColor);

        $('#planid').val( $(this).data('planid'));
		$('#termchosen').val(termChosen);
        //$('#modemSelection<?php //echo ($connection_tab_link);?>').removeClass('hidden');
    });
	
	$(document).on('click','.card-tabs-bar a',function(e){
	 	$('#planSignup .planSignup-inner').hide();
	 });
	
	$('.term').change(function(){
		//var termChoice = $(this).val();
		var termChosen = $(this).val();
		var setupCost = $(this).children('option:selected').attr('data-setup-cost');
		var termPrice = $(this).children('option:selected').attr('data-total-cost');
		var monthlyPrice = $(this).children('option:selected').attr('data-monthly-cost');
		$(this).parent().siblings('.monthlycost').children('strong').html('$' + monthlyPrice);
		$(this).parent().siblings('.totalcost').children('span').html('$' + termPrice);
		$(this).parent().siblings('.setupcost').children('span').html('$' + setupCost);
		//alert(termChoice);
	});
	
	$('.wire').change(function(){
		//var termChoice = $(this).val();
		var wireChosen = $(this).val();
		var termPrice = $(this).children('option:selected').attr('data-total-cost');
		$(this).parent().siblings('.monthlycost').children('strong').html('$' + wireChosen);
		$(this).parent().siblings('.totalcost').children('span').html('$' + termPrice);
		//alert(termChoice);
	});
	

    //Signup field check to see if all fields are filled in
    $('#planSelector').bind('submit', function(e){
        e.preventDefault();

        if( $('#name-field').val()  && ($('#email-field').val().indexOf("@") > 0) && $('#phone-field').val() ) {
            var data = $('#planSelector').serialize();
			///wp-admin/admin-ajax.php?action=lifewireless_plan_request
            $.post('/plan-request', data, function( response ) {
              $('.form-process').fadeTo('fast',1, function(){
                  $('#signup-step1').fadeOut(1000).promise().done(function(){
                      var emailAddress = $('#email-field').val();
                      $('#signup-step1').addClass('hidden');
                      $('#signup-step2').removeClass('hidden');
                      $('#emailAddress').html(emailAddress);
					  if(typeof ga !== 'undefined' ){
						ga('send', 'event', 'plan chooser', 'submit', 'Plan request from ' +document.location.pathname);
					}
                  });
              });
            }).fail(function(xhr) {
				$('#planSelector').html('<div class="cta-response errormsg alert"><h4 class="nobottommargin">Oh no!</h4><p>Sorry, an error occurred. An error log has been sent to our development team for review. If you do not hear from us in 48 hours, please call 1300 080 820.</p></div>');
				$('#form-condition-submit .submit').html('Send email').removeAttr('disabled');
				//console.log('A form was not sent due to the following error: ' + xhr.status + ' ' + xhr.statusText + '\n We were able to retrieve the following data: ' + data + '\n More details: ' +xhr.getAllResponseHeaders());

				_LTracker.push('A form on MeshTelco was not sent due to the following error: ' + xhr.status + ' ' + xhr.statusText + '\n We were able to retrieve the following data: ' + data + '\n More details: ' +xhr.getAllResponseHeaders());
			  })
			  ;

        }else{
        }
    });

    //$('#signup-step2 .button').click(function(){
        //$('#signup-step2').addClass('hidden');
        //$('#signup-step3').removeClass('hidden');
    //});


});
