<?php /**
 * Template Name: Contact
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
$pageID = get_the_ID();
//Standard field includes for SEO, Hero, and intro
include 'partials/fields.php';
?>

<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>
	<?php include 'partials/header.php';?>
</head>

<body class="stretched <?php echo $page_class; ?>" data-loader-color="#E30160">
	<div class="loader-holder">
		<img class="loader" src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/loader.gif"/>
	</div>
	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TH927RC"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->
	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">

		<?php include 'partials/menu.php';
		//checks whether the featured image is set and then sets it to the hero
		if (has_post_thumbnail($pageID) ):
		$image = wp_get_attachment_image_src( get_post_thumbnail_id( $pageID ), 'single-post-thumbnail' );
		endif; ?>

        <!-- Hero image and page title
        ============================================= -->
        <?php include 'partials/pagetitle.php';?>
       
		<!-- Content
		============================================= -->
		<section id="content">
			<div class="content-wrap">
            	<div id="contact"  class="section nopadding nomargin nobottomborder">
					<div class="container clearfix nobottommargin">
                    <!-- Contact Form
                    ============================================= -->
                        <div class="row clearfix topmargin-sm">
                          
							<div class="column col_half nobottommargin center">
								<?php include 'partials/cta.php';?>
							</div>
				

                        <!-- Google Map
                        ============================================= -->
                        <div class="col_half col_last">

							<div class="heading-block nobottommargin">
								<h2 class="nobottommargin center">Find us</h2>
							</div> 
							<?php
							$email = get_field("email", $pageID );
							$phone = get_field("phone", $pageID );
							$address = get_field("address", $pageID );
							?>
							<div class="contactDetails">
								<i class="icon-email3"></i><a href="mailto:<?php echo $email;?>"><?php echo $email;?></a><br/>
								<i class="icon-call"></i><span><?php echo $phone;?></span><br/>
								<i class="icon-location-arrow"></i><span><?php echo $address; ?></span>
							</div>

                                <!-- Contact Info End -->
                            <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDxFnZ6KYVwIVrFmcUdMU2qgSRQiCkW-NQ&sensor=false&extension=.js"></script>
                            <script>
                                google.maps.event.addDomListener(window, 'load', init);
                                var map;
                                function init() {
                                    // If document (your website) is wider than 480px, isDraggable = true, else isDraggable = false
                                    var isDraggable = $(document).width() > 480 ? true : false;
                                    var mapOptions = {
                                        center: new google.maps.LatLng(-37.8986431,145.0880675),
                                        zoom: 16,
                                        zoomControl: true,
                                        zoomControlOptions: {
                                            style: google.maps.ZoomControlStyle.DEFAULT,
                                        },
                                        disableDoubleClickZoom: true,
                                        mapTypeControl: true,
                                        mapTypeControlOptions: {
                                            style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
                                        },
                                        scaleControl: true,
                                        scrollwheel: isDraggable,
                                        panControl: true,
                                        streetViewControl: true,
                                        draggable: isDraggable,
                                        overviewMapControl: true,
                                        overviewMapControlOptions: {
                                            opened: false,
                                        },
                                        mapTypeId: google.maps.MapTypeId.ROADMAP,
                                        styles: [
    {
        "featureType": "administrative",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#ca2031"
            }
        ]
    },
    {
        "featureType": "administrative.province",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "administrative.land_parcel",
        "elementType": "geometry",
        "stylers": [
            {
                "visibility": "on"
            }
        ]
    },
    {
        "featureType": "administrative.land_parcel",
        "elementType": "labels.text",
        "stylers": [
            {
                "visibility": "simplified"
            }
        ]
    },
    {
        "featureType": "landscape",
        "elementType": "all",
        "stylers": [
            {
                "saturation": -100
            },
            {
                "lightness": 65
            },
            {
                "visibility": "on"
            }
        ]
    },
    {
        "featureType": "landscape.man_made",
        "elementType": "geometry",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "all",
        "stylers": [
            {
                "saturation": -100
            },
            {
                "lightness": 51
            },
            {
                "visibility": "simplified"
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "labels.icon",
        "stylers": [
            {
                "saturation": "62"
            },
            {
                "lightness": "-4"
            },
            {
                "gamma": "2.22"
            },
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "all",
        "stylers": [
            {
                "saturation": -100
            },
            {
                "visibility": "simplified"
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#dd4b39"
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#ffffff"
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "labels.text.stroke",
        "stylers": [
            {
                "weight": "4.41"
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "all",
        "stylers": [
            {
                "saturation": -100
            },
            {
                "lightness": 30
            },
            {
                "visibility": "on"
            }
        ]
    },
    {
        "featureType": "road.local",
        "elementType": "all",
        "stylers": [
            {
                "saturation": -100
            },
            {
                "lightness": 40
            },
            {
                "visibility": "on"
            }
        ]
    },
    {
        "featureType": "transit",
        "elementType": "all",
        "stylers": [
            {
                "saturation": -100
            },
            {
                "visibility": "simplified"
            }
        ]
    },
    {
        "featureType": "transit.station",
        "elementType": "labels.icon",
        "stylers": [
            {
                "hue": "#00ecff"
            },
            {
                "saturation": "52"
            },
            {
                "lightness": "-6"
            },
            {
                "gamma": "1.07"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "geometry",
        "stylers": [
            {
                "lightness": -25
            },
            {
                "saturation": -97
            },
            {
                "color": "#019eab"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "labels",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "lightness": -25
            },
            {
                "saturation": -100
            }
        ]
    }
],
                                    }
                                    var mapElement = document.getElementById('google_map');
                                    var map = new google.maps.Map(mapElement, mapOptions);
                                    var locations = [
										['MeshTelco', 'Connecting Your World', '1800 080 820', 'info@meshtelco.com.au', 'www.meshtelco.com.au', -37.8986431,145.0880675, '<?php echo esc_url( get_template_directory_uri() ); ?>/images/map-marker.png']
                                    ];
                                    for (i = 0; i < locations.length; i++) {
										if (locations[i][1] =='undefined'){ description ='';} else { description = locations[i][1];}
										if (locations[i][2] =='undefined'){ telephone ='';} else { telephone = locations[i][2];}
										if (locations[i][3] =='undefined'){ email ='';} else { email = locations[i][3];}
										if (locations[i][4] =='undefined'){ web ='';} else { web = locations[i][4];}
										if (locations[i][7] =='undefined'){ markericon ='';} else { markericon = locations[i][7];}
                                        marker = new google.maps.Marker({
                                            icon: markericon,
                                            position: new google.maps.LatLng(locations[i][5], locations[i][6]),
                                            map: map,
                                            title: locations[i][0],
                                            desc: description,
                                            tel: telephone,
                                            email: email,
                                            web: web
                                        });

										var infowindow = new google.maps.InfoWindow();

										google.maps.event.addListener(marker, 'click', function() {
										  infowindow.setContent('<p><strong>'+ marker.title+'</strong><em>'+ marker.desc +'</em>'+ marker.tel +'<br/><a href="mailto:"'+ marker.email +'">'+ marker.email +'</a></p>');
										  infowindow.open(map, this);
										});

                                    link = '';     }

                                    }

                            </script>
                            <style>
                                #google_map {
                                    height:475px;
                                    width:100%;
                                }
                                @media (max-width: 767px) {
                                    #google_map {
                                        height:300px;
                                        top:20px;
                                    }
                                }
                                .gm-style-iw * {
                                    display: block;
                                    width: 100%;
                                }
                                .gm-style-iw h4, .gm-style-iw p {
                                    margin: 0;
                                    padding: 0;
                                }
                                .gm-style-iw a {
                                    color: #4272db;
                                }
                            </style>
                            <div id="google_map"></div><!-- Google Map End -->
                            <div class="clear"></div>

                        </div>
                    </div>
                </div>
             </div>

		</section><!-- #content end -->

		<?php include 'partials/footer.php';?>

	</div><!-- #wrapper end -->
	<!-- Footer Scripts
	============================================= -->
	<script type="text/javascript" src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/functions.js"></script>

</body>
</html>
