<?php /**
 * Template Name: Home
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */	
$pageID = 5;
//Standard field includes for SEO, Hero, and intro
include 'partials/fields.php';

$liteHome = 355;
$planPrice = get_field('monthly_cost',$liteHome);
$developers_title = get_field('developers_title',$pageID);
$developers_text = get_field('developers_text',$pageID);
$developers_button_text = get_field('developers_button_text',$pageID);

$slideTitle1 = get_field('slide_title_1',$pageID);
$slideText1 = get_field('slide_text_1',$pageID);
$slideImage1 = get_field('slide_image_1',$pageID);
$slideLink1 = get_field('slide_link_1',$pageID);
$slideCaptionAlignment1 = get_field('slide_caption_alignment_1',$pageID);
$slideCaptionColor1 = get_field('slide_caption_color_1',$pageID);
$slideButtonColor1 = get_field('slide_button_color_1',$pageID);
$slideButtonText1 = get_field('slide_button_text_1',$pageID);


$slideTitle2 = get_field('slide_title_2',$pageID);
$slideText2 = get_field('slide_text_2',$pageID);
$slideImage2 = get_field('slide_image_2',$pageID);
$slideLink2 = get_field('slide_link_2',$pageID);
$slideCaptionAlignment2 = get_field('slide_caption_alignment_2',$pageID);
$slideCaptionColor2 = get_field('slide_caption_color_2',$pageID);
$slideButtonColor2 = get_field('slide_button_color_2',$pageID);
$slideButtonText2 = get_field('slide_button_text_2',$pageID);


$slideTitle3 = get_field('slide_title_3',$pageID);
$slideText3 = get_field('slide_text_3',$pageID);
$slideImage3 = get_field('slide_image_3',$pageID);
$slideLink3 = get_field('slide_link_3',$pageID);
$slideCaptionAlignment3 = get_field('slide_caption_alignment_3',$pageID);
$slideCaptionColor3 = get_field('slide_caption_color_3',$pageID);
$slideButtonColor3 = get_field('slide_button_color_3',$pageID);
$slideButtonText3 = get_field('slide_button_text_3',$pageID);


?>

<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>
	<?php include 'partials/header.php';?>
</head>

<body class="stretched home no-transition">
	
	<div class="loader-holder">
		<img class="loader" src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/loader.gif"/>
	</div>
	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">

		<?php include 'partials/menu.php';
		
		//checks whether the featured image is set and then sets it to the hero
		if (has_post_thumbnail(5 ) ): 
		$image = wp_get_attachment_image_src( get_post_thumbnail_id( 5 ), 'single-post-thumbnail' ); 
		endif; ?>
       

		<!-- Hero image and page title
        ============================================= -->
        <?php include 'partials/slider.php';?>
		
       

		<!-- Content
		============================================= -->
        <section id="content">
			<div class="content-wrap">
				<?php 
				//include 'partials/addresscheck.php';
				include 'partials/pagelinks.php';
				include 'partials/developers.php';
				?>
				<section id="contact" class="section cta nomargin nobottomborder">
					<div class="container clearfix nobottomborder">
					   <div class="col_one_fourth">
						</div>
							<div class="column col_half col_last nobottommargin center">
								<?php include 'partials/cta.php';?>
							</div>
						<div class="col_one_fourth col_last">
						</div>
					</div>
				</section>
			</div>
		</section><!-- #content end -->

		<?php include 'partials/footer.php';?>

	</div><!-- #wrapper end -->
</body>
</html>