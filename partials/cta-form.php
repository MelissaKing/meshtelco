<div class="cta-form-result" data-notify-type="success" data-notify-msg=""></div>
<div class="cta-response"></div>
<form class="nobottommargin contactForm" id="ctaform" name="ctaform" action="" method="post">
	<div class="form-process"></div>

	<div class="col_full clearfix">
	   <div class="col_<?php if(!in_array($pageID,array(378))){?>half<?php }else{ ?>full<?php } ?> nobottommargin">
			<input type="text" id="contactform_name" name="name" value="" class="sm-form-control required" placeholder="Name*" required/>
		</div>
		<div class="col_half col_last nobottommargin">
		<!--Checks if page is residential, homes landing, or games landing. Covers live and development IDs -->
		<?php if(!in_array($pageID,array(378))){?>
			<input type="text" id="contactform_business" name="business" value="" class="sm-form-control" placeholder="Business Name" />
		<?php }?>
		</div>
	</div>

	<div class="col_full clearfix">
	   <div class="col_half nobottommargin">
			<input type="email" id="contactform_email" name="email" value="" class="required email sm-form-control" placeholder="Email*" required/>
		</div>
		<div class="col_half col_last nobottommargin">
			 <input type="text" id="contactform_phone" name="phone" value="" class="sm-form-control" placeholder="Phone*" required/>
		</div>
	</div>


	<div class="col_full clearfix">
		<select name="time" class="sm-form-control" id="time">
			<option disabled selected>Best contact time</option>
			<option name="Any time" value="none">Any time</option>
			<option name="Morning" value="morning">Morning</option>
			<option name="Afternoon" value="afternoon">Afternoon</option>
			<option name="Evening" value="evening">Evening</option>
		</select>
   </div>

	<div class="col_full clearfix">
		<textarea cols="3" name="comments" id="comments" class="sm-form-control" placeholder="Got any comments?"></textarea>
	</div>
	
	<div class="col_full hidden nobottommargin">
		<input type="text" id="address" name="address" value="" class="sm-form-control" />
	</div>

	<div class="col_full hidden nobottommargin">
		<input type="text" id="contactform_botcheck" name="contactform_botcheck" value="" class="sm-form-control" />
	</div>

	<div class="col_full clearfix">
		<button name="submit" type="submit" id="submit-button" tabindex="5" value="Submit" class="button button-large nomargin button-red pull-right button-3d"><?php 
			if(!(empty($cta_button_text))){
				echo $cta_button_text;
			}else{
				echo 'Request a Callback';
			}
			?></button>
	</div>
</form>