<?php 
$parent_title = get_the_title( $post->post_parent );
$current_page_title = get_the_title($post);
	// Checks if the page parent title matches the current page (ie top level page), if it doesn't, show the breadcrumb
if ( $parent_title != $current_page_title ) {
	echo '<div class="breadcrumb container">';
		echo '<a href="' . esc_url( get_permalink( $post->post_parent ) ) . '" alt="' . esc_attr( $parent_title ) . '"><strong>' . $parent_title . '</strong></a>&nbsp;&nbsp;»&nbsp;&nbsp;';
		wp_list_pages( array (
			'child_of' => $post->post_parent,
			'title_li' => ''
			)
		);
echo '</div>';
}else{
	
	echo '<div class="breadcrumb container">';
		echo '<a href="' . esc_url( get_permalink( $post->post_parent ) ) . '" alt="' . esc_attr( $parent_title ) . '"><strong>' . $parent_title . '</strong></a>&nbsp;&nbsp;»&nbsp;&nbsp;';
		wp_list_pages( array (
			'child_of' => $pageID,
			'title_li' => ''
			)
		);
echo '</div>';
	
}	?>