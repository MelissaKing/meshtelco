<?php
$page_title = get_field("page_title", $pageID );
$meta_description = get_field("seo_meta_description", $pageID);
$hero_button_text = get_field("cta_button_text", $pageID );
$page_subtitle = get_field("page_subtitle", $pageID );
$intro_image = get_field("intro_image", $pageID );
$intro_heading = get_field("intro_subtitle", $pageID );
$intro_text = get_field("intro_text", $pageID );
$page = get_the_title($post->post_parent);
$page_nospace = strtolower(str_replace(" ", "-", $page));
$page_class = preg_replace("/[^a-zA-Z0-9]+/", "", $page_nospace);
?>