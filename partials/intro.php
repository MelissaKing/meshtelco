 <?php
	if (empty($intro_text)==false){?>
	<section id="intro" class="section nobottommargin noborder nopadding">
		<div class="container clearfix notopmargin">
			<div class="row clearfix nomargin">
				<div class="<?php
					if (empty($intro_image)==false){
						echo "col_half clearfix";
					}else if(!(in_array($pageID,array(344,213)))){
						echo "center";
					}else{
						echo "col_full";
					}?> heading-block nobottommargin ">
					
					
					<h2><?php echo $intro_heading;?></h2>
					<p><?php echo $intro_text;?></p>
				<?php
				if (empty($intro_image)==false){ ?> 
				</div>
				<div class="col_half center col_last nobottommargin">
						
					<img src="<?php echo $intro_image[url];?>" alt="<?php echo $intro_image[alt];?>"/>
					<?php } ?>
				</div>
			</div>
		</div>
	</section>
<?php } ?>	