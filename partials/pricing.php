
<?php if(in_array($pageID,array(769))){ ?>

<section class="section nobottomborder nobottompadding header-stick" id="inbound-pricing">
	<div class="container">
		<div class="col_half center">
			<h3>Inbound numbers starting from just</h3>
			<span>$18.88<sup>*</sup></span>
			<p class="disclaimer"><sup>*</sup>Excluding GST</p>
		</div>
		<div class="col_half col_last">
			<a class="topmargin-sm scroll button button-red button-xlarge button-fullwidth button-rounded" href="#contact">ENQUIRE NOW</a>
		</div>
	</div><br>
</section>
<?php }else{ ?>
<section class="section nobottomborder nomargin nobottompadding" id="pricing">
	<div class="container clearfix nobottomborder">
		<div class="row nobottommargin">
			<div class="heading-block nobottomborder center topmargin-sm">
				<h2>bundled Pricing</h2>
				<p>Our VoIP plans are available as an addon to our <a href="internet">internet plans</a></p>
			 </div>
			<div class="pricing nobottommargin clearfix">
				<div class="col_half">
					<div class="pricing-box pricing1">
						<div class="pricing-title">
							<h3>VoIP STANDARD</h3>

						</div>
						<div class="pricing-price">
							
							<span class="price-amount">+<sup>&dollar;</sup>10<sub>/mo</sub></span>
							
						</div>
						<div class="pricing-features">
							<ul>
								<li>Unlimited Local &amp; National Calls</li>
								<li>8.4c/min Mobile Calls</li>
								<li>26c calls to 13/1300</li>
								<li><a href="<?php echo esc_url( get_template_directory_uri() ); ?>/images/MeshTelco_international_callrates.pdf">International Call Rates</a></li>
							</ul>
						</div>
						<div class="pricing-action">
							<a href="/internet" class="button button-xlarge button-red button-rounded">BUY NOW</a>
						</div>
					</div>
				</div>
				<div class="col_half col_last">
					<div class="pricing-box pricing2">
						<div class="pricing-title">
							<h3>VoIP PREMIUM</h3>
						</div>
						<div class="pricing-price">
							<span class="price-amount">+<sup>&dollar;</sup>20<sub>/mo</sub></span>
						</div>
						<div class="pricing-features">
							<ul>
								<li>Unlimited Mobile, Local, &amp; National Calls</li>
								<li>8.4c/min Mobile Calls</li>
								<li>26c calls to 13/1300</li>
								<li><a href="<?php echo esc_url( get_template_directory_uri() ); ?>/images/MeshTelco_international_callrates.pdf">International Call Rates</a></li>
							</ul>
						</div>
						<div class="pricing-action">
							<a href="/internet" class=" button button-xlarge button-red button-rounded">BUY NOW</a>
						</div>

					</div>
				</div>

				<p class="center disclaimer"></p>
			</div>
		</div>
	</div>
</section>
<?php } ?> 
