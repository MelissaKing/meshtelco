<!-- Required for FB footer widget -->
<div id="fb-root"></div>

<!-- Header
============================================= -->
<header id="header" class="page-section sticky clearfix">
	<?php $phone = get_field("phone", 51 ); 
	$phone_nospace = str_replace(" ", "", $phone);
	?>
	<div id="header-wrap">
		<div class="container clearfix">

			<!-- Logo
			============================================= -->
			<div id="logo">
				<?php canvas_the_custom_logo(); ?>
			</div><!-- #logo end -->

			<!-- Primary Navigation
			============================================= -->
			<nav id="primary-menu">
				<a class="menu-icon">
					<i class="icon icon-reorder"></i>
				</a>
				<?php  wp_nav_menu( array( 
				'canvas' => 'top-header' 
				)); ?>
			   

			   <a href="tel:<?php echo $phone_nospace?>" onClick="ga('send', 'event', 'Phone icon', 'click', 'Call via Phone Icon');" class="button button-large button-transparent"><div class="fbox fbox-icon"><i class="icon-call icon-2x"></i></div><span class="phone"><?php echo $phone;?></span></a>
			</nav><!-- #primary-menu end -->
		</div>
	</div>
</header><!-- #header end -->