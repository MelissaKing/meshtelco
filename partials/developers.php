	<div id="developers" class="section noborder nopadding dark">
		<div class="container clearfix">
			<div class="row clearfix">
				<div class="col_half center nobottommargin">
					<div class="heading-block center">
						<h3>Our Clients</h3>
					</div>
					<ul class="clients-grid grid-3 nobottommargin clearfix">

						<li><a href="https://www.anytimefitness.com.au/" target="_blank"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/logo-atf.png" alt="Anytime Fitness Australia"></a></li>
						<li><a href="http://au.ufc.com/" target="_blank"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/logo-ufc.png" alt="UFC Australia"></a></li>
						<li><a href="http://www.serviced-apartments.com.au/" target="_blank"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/logo-parkave.png" alt="Park Avenue Accommodation Group"></a></li>
						
					</ul>
					<div class="heading-block center">
						<h3>Our Sister Products</h3>
					</div>
					
					<ul class="clients-grid grid-3 nobottommargin clearfix">

						<li><a href="https://lifewireless.com.au/" target="_blank"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/logo-lifewireless.png" alt="Life Wireless Fixed Wireless internet"></a></li>
						<li><a href="http://meshdigital.com.au/" target="_blank"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/logo-meshdigital.png" alt="MeshDigital Digital Signage"></a></li>
						<li><a href="http://www.meshone.com.au/" target="_blank"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/logo-meshone.png" alt="MeshOne Mesh Wireless Networks"></a></li>
						
					</ul>
				</div>
				<div class="col_half col_last topmargin-sm">
					<div class="heading-block center">
					<h2><?php echo $developers_title?></h2>
						<p><?php echo $developers_text?></p>
						<a href="business/embedded-networks" class="button button-white button-xlarge"><?php echo $developers_button_text?> <i class="icon-chevron-sign-right"></i></a>
					</div>
				</div>
			</div>
		</div>
	</div>