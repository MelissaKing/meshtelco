
 	<?php 
	 $testimonial_title = get_field("testimonial_title", $pageID );
	 $testimonial_image_1 = get_field("testimonial_image_1", $pageID );
	$testimonial_text_1 = get_field("testimonial_text_1", $pageID );
	 $testimonial_text_2 = get_field("testimonial_text_2", $pageID );
	 $testimonial_image_2 = get_field("testimonial_image_2", $pageID );
	 $testimonial_text_3 = get_field("testimonial_text_3", $pageID );
	 $testimonial_image_3 = get_field("testimonial_image_3", $pageID );
	 ?>
  <?php
	if (empty($testimonial_text_1)==false){?>
<section class="slider-container bottommargin" id="testimonials">
	<div class="row container">
		
		<div class="heading-block center topmargin">
			<h2><?php echo $testimonial_title;?></h2>
		</div>
		<ul class="slider slider-list nothovered">
		
			<li>
			<?php if (empty($testimonial_image_1)==false){ ?>
				<div class="col_one_fourth testi-image">
					<img src="<?php echo $testimonial_image_1[url];?>" alt="<?php echo $testimonial_image_1[alt];?>">
				</div>
				<div class="col_three_fourth col_last">
					<?php echo $testimonial_text_1;?>
				</div>
			<?php }else{ ?>
				<div class="col_full">
					<?php echo $testimonial_text_1;?>
				</div>
			<?php } ?>
			</li>
		<?php
		if (empty($testimonial_text_2)==false){?>
			<li>
			<?php if (empty($testimonial_image_2)==false){ ?>
			
				<div class="col_one_fourth testi-image">
					<img src="<?php echo $testimonial_image_2[url];?>" alt="<?php echo $testimonial_image_2[alt];?>">
				</div>
				<div class="col_three_fourth col_last">
					<?php echo $testimonial_text_2;?>
				</div>
			<?php }else{ ?>
				<div class="col_full">
					<?php echo $testimonial_text_2;?>
				</div>
			<?php } ?>
			</li>
		<?php } 
		if (empty($testimonial_text_3)==false){?>
		
			<li>
			<?php if (empty($testimonial_image_3)==false){ ?>
			
				<div class="col_one_fourth testi-image">
					<img src="<?php echo $testimonial_image_3[url];?>" alt="<?php echo $testimonial_image_3[alt];?>">
				</div>
				<div class="col_three_fourth col_last">
					<?php echo $testimonial_text_3;?>
				</div>
			<?php }else{ ?>
				<div class="col_full">
					<?php echo $testimonial_text_3;?>
				</div>
			<?php } ?>
				
			</li>
			<?php } ?>
		</ul>
	</div>
</section>
<?php } ?>
            