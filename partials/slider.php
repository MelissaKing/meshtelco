<section id="homeSlider" class="slider-container">
	<div class="row container">
		<ul class="slider slider-list nothovered clearfix">
			<li><img src="<?php echo $slideImage1['url']; ?>" alt="<?php echo $slideImage1['alt']; ?>"/>
				
				<a class="clearfix caption caption-<?php echo $slideCaptionAlignment1;?>" href="<?php if(strstr($slideLink1,'#')){echo($slideLink1);}else{echo (get_site_url() .$slideLink1);} ?>">

					<div class="button button-red button-xlarge button-fullwidth"><?php echo $slideButtonText1;?></div>
				</a>
				
			</li>
			<li><img src="<?php echo $slideImage2['url']; ?>" alt="<?php echo $slideImage2['alt']; ?>"/>
				<?php 
			//Checks if there's text on the button
			if (empty($slideButtonText2) == false){ ?>
					<a class="clearfix <?php if(strstr($slideLink2,'#')){echo('scroll');} ?> caption caption-<?php echo $slideCaptionAlignment2;?>" href="<?php if(strstr($slideLink2,'#')){echo($slideLink2);}else{echo (get_site_url() .$slideLink2);} ?>">
						<h2 class="nobottommargin"><?php echo $slideTitle2;?></h2>
						<p><?php echo $slideText2;?></p>
						<div class="button button-red button-xlarge"><?php echo $slideButtonText2;?></div>
					</a>
				<?php } ?>
			</li>

			<li><img src="<?php echo $slideImage3['url']; ?>" alt="<?php echo $slideImage3['alt']; ?>"/>
				<div class="caption products">
					<div class="col_one_fourth"></div>
					<a href="http://www.meshone.com.au" target="_blank" class="col_one_fourth"></a>
					<a href="http://www.lifewireless.com.au" target="_blank" class="col_one_fourth"></a>
					<a href="http://www.meshdigital.com.au" target="_blank" class="col_one_fourth col_last"></a>
				</div>
				
			
				<?php 
			//Checks if there's text on the button
			if (empty($slideButtonText3) == false){ ?>
					<a class="clearfix <?php if(strstr($slideLink3,'#')){echo('scroll');} ?> caption caption-<?php echo $slideCaptionAlignment3;?>" href="<?php if(strstr($slideLink3,'#')){echo($slideLink3);}else{echo (get_site_url() .$slideLink3);} ?>">
						<h2 class="nobottommargin"><?php echo $slideTitle3;?></h2>
						<p><?php echo $slideText3;?></p>
						<div class="button button-red button-xlarge"><?php echo $slideButtonText3;?></div>
					</a>
				<?php } ?>
			</li>
		</ul>
		
	</div>
	<div class="slider-bg" style="background-color: #efefef;display:block;position:absolute;height:100%;width:100%;top:0;"></div>
</section>