
<?php

$features_title = get_field("features_title", $pageID );
$feature_icon_1 = get_field("feature_icon_1", $pageID );
$feature_title_1 = get_field("feature_title_1", $pageID );
$feature_text_1 = get_field("feature_text_1", $pageID );
$feature_icon_2 = get_field("feature_icon_2", $pageID );
$feature_title_2 = get_field("feature_title_2", $pageID );
$feature_text_2 = get_field("feature_text_2", $pageID );
$feature_icon_3 = get_field("feature_icon_3", $pageID );
$feature_title_3 = get_field("feature_title_3", $pageID );
$feature_text_3 = get_field("feature_text_3", $pageID );
$feature_icon_4 = get_field("feature_icon_4", $pageID );
$feature_title_4 = get_field("feature_title_4", $pageID );
$feature_text_4 = get_field("feature_text_4", $pageID );

 if (empty($feature_title_4) == false){
	 $colwidth="fourth";
 } else{
	 $colwidth="third";
 }

if (empty($feature_title_1)==false){
?>

<section id="features" class="section nobottommargin features noborder">
	<div class="container clearfix nobottommargin notopmargin">
		<div class="row clearfix nomargin">
			<div class="col_full">
				<div class="heading-block center">
					<h2><?php echo $features_title;?></h2>

				</div>
			</div>
		</div>
		<div class="row clearfix nomargin">
			<div class="col_one_<?php echo $colwidth; ?>">
				<div class="feature-box fbox-center fbox-effect">
					<div class="fbox-icon">
						<i class="icon-<?php echo $feature_icon_1;?>"></i>
					</div>
					<h3><?php echo $feature_title_1;?></h3>
					<p><?php echo $feature_text_1;?></p>
				</div>
			</div>
			<div class="col_one_<?php echo $colwidth; ?>">
				<div class="feature-box fbox-center fbox-effect">
					<div class="fbox-icon">
						<i class="icon-<?php echo $feature_icon_2;?>"></i>
					</div>
					<h3><?php echo $feature_title_2;?></h3>
					<p><?php echo $feature_text_2;?></p>
				</div>
			</div>
			<div class="col_one_<?php echo $colwidth; ?> <?php if (empty($feature_title_4) == true){ echo 'col_last'; } ?>">
				<div class="feature-box fbox-center fbox-effect">
					<div class="fbox-icon">
						<i class="icon-<?php echo $feature_icon_3;?>"></i>
					</div>
					<h3><?php echo $feature_title_3;?></h3>
					<p><?php echo $feature_text_3;?></p>
				</div>
			</div>
			
			<?php 
			//Checks if Icon 4 is set, and if so makes the fourth icon box
			if (empty($feature_title_4) == false){ ?>
			<div class="col_one_<?php echo $colwidth; ?> col_last">
				<div class="feature-box fbox-center fbox-effect">
					<div class="fbox-icon">
						<i class="icon-<?php echo $feature_icon_4;?>"></i>
					</div>
					<h3><?php echo $feature_title_4;?></h3>
					<p><?php echo $feature_text_4;?></p>
				</div>
			</div>
			<?php } ?>
			
		</div>
		
		<?php 
		//adds two more rows of features for 3CX page							
		if($pageID == 767){?>
		<div class="row clearfix nomargin">
			<div class="col_one_fourth">
				<div class="feature-box fbox-center fbox-effect">
					<div class="fbox-icon">
						<i class="icon-line2-size-actual"></i>
					</div>
					<h3>CALL CONFERENCING</h3>
					<p></p>
				</div>
			</div>
			<div class="col_one_fourth">
				<div class="feature-box fbox-center fbox-effect">
					<div class="fbox-icon">
						<i class="icon-line2-bubbles"></i>
					</div>
					<h3>UNIFIED MESSAGING</h3>
					<p>receive voicemail and fax via email</p>
				</div>
			</div>
			<div class="col_one_fourth">
				<div class="feature-box fbox-center fbox-effect">
					<div class="fbox-icon">
						<i class="icon-line2-music-tone-alt"></i>
					</div>
					<h3>MUSIC-ON-HOLD</h3>
					<p>and business adverts/announcements</p>
				</div>
			</div>
			<div class="col_one_fourth col_last">
				<div class="feature-box fbox-center fbox-effect">
					<div class="fbox-icon">
						<i class="icon-line2-clock"></i>
					</div>
					<h3>SET PRESENCE</h3>
					<p>so colleagues know your availability</p>
				</div>
			</div>
		</div>
		
		<div class="row clearfix nomargin">
			<div class="col_one_fourth">
				<div class="feature-box fbox-center fbox-effect">
					<div class="fbox-icon">
						<i class="icon-line2-link"></i>
					</div>
					<h3>3CX BRIDGES</h3>
					<p>connect branch offices seamlessly</p>
				</div>
			</div>
			<div class="col_one_fourth">
				<div class="feature-box fbox-center fbox-effect">
					<div class="fbox-icon">
						<i class="icon-line2-call-out"></i>
					</div>
					<h3>CALL FORWARDING</h3>
					<p>with advanced rules by caller ID, time and type of call (Follow-Me)</p>
				</div>
			</div>
			<div class="col_one_fourth">
				<div class="feature-box fbox-center fbox-effect">
					<div class="fbox-icon">
						<i class="icon-chat-3"></i>
					</div>
					<h3>INSTANT MESSAGING</h3>
					<p>(chat) via call assistant</p>
				</div>
			</div>
			<div class="col_one_fourth col_last">
				<div class="feature-box fbox-center fbox-effect">
					<div class="fbox-icon">
						<i class="icon-line-mail"></i>
					</div>
					<h3>OUTLOOK/SALESFORCE INTEGRATION</h3>
					<p>launch calls directly from your favourite CRM</p>
				</div>
			</div>
		</div>

		<?php	}?>
		



	</div>
</section>
<?php } ?>