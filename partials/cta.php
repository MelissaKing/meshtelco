
	<div id="section-buy" class="heading-block nobottomborder">
		<?php
		$contactpageID = 51;
		$phone = get_field("phone", $contactpageID );
		$cta_title = get_field("cta_title", $pageID );
		$cta_text = get_field("cta_text", $pageID );
		$cta_button_text = get_field("cta_button_text", $pageID );
		?>
		<h2><?php echo $cta_title;?></h2>
		<h3 class="cta-phone"><i class="icon-call"></i> <?php echo $phone;?></h3>
		<?php echo $cta_text;?>
	 </div>
	<?php include('cta-form.php');?>



