<!-- External JavaScripts
============================================= -->
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/meshtelco/planreview.js"></script>

<?php
//switches the connection type for classes, which change the colours of the pricing boxes and tabs
function connectionType($connection_type){
switch (true){
		case stristr($connection_type,"adsl"):
			echo ("adsl");
			break;
		case stristr($connection_type,"mbe"):
			echo ("mbe");
			break;
		case stristr($connection_type,"efm"):
			echo ("efm");
			break;
		case stristr($connection_type,"nbn"):
			echo ("nbn");
			break;
		case stristr($connection_type,"business"):
			echo ("busnbn");
			break;
	}

}?>

<!-- Hidden Error Messages
============================================= -->
<div id="address-message" data-notify-position="top-full-width" data-notify-type="error" data-notify-msg="<i class='icon-remove-sign'></i> Whoops! You need to enter your address!"></div>
<div id="details-message" data-notify-position="enquire-full-width" data-notify-type="error" data-notify-msg="<i class='icon-remove-sign'></i> Please enter all your details!"></div>


<!-- Content
============================================= -->
<?php 

$request_path = $_SERVER['REQUEST_URI'];
$path = explode("/", $request_path); // splitting the path
$last = end($path);
$last = prev($path); ?>
<!--if (stristr($last,"ethernet") == false){
	include 'addresscheck.php';
}-->

<section id="planChooser">
	<div class="container clearfix notopmargin">
	<!--< ?php if (stristr($last,"ethernet") == true){?>-->
		<h2 class="center">Browse our plans</h2>
	<!--< ?php }?>-->
		<div class="plan-preloader">
			<img class="loader" src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/loader.gif"/>
		</div>
		<div class="container clearfix">
			<!--<div class="col_full clearfix business-checkbox">
				<input type="checkbox" class="pull-left rightmargin-sm" name="form-business-address" id="form-business-address"/><label for="form-business-address" class="pull-left">I am a business customer &nbsp;</label><i class="icon-question-sign icon-lg" data-notify-type="info" data-notify-msg="<i class=icon-info-sign></i> Some types of connection are only available at business addresses." onclick="SEMICOLON.widget.notifications(this); return false;"></i>
			</div>-->
			<div class="alert alert-success center hidden"><h4 class="nobottommargin"><i class="icon-ok-sign icon-lg"></i> Good news! The following plans are available at your address:</h4></div>
			
					<?php

					//Sets up query to get connection types, and then generate the tabs based on the results (ie, adsl, etc)
					$connection_query = new WP_Query();
					$connection = get_page_by_title('Plans');
					$args = array(
						'post_type' => 'page',
						'post_parent' => $connection->ID,
						'ignore_sticky_posts' => true,
						'fields' => 'ids',
						'order' =>'ASC',
						'orderby' => 'menu_order'
					);
					$connection_pages = $connection_query->query($args);
					//loop to generate the tabs. Checks if it's a business connection and shows/hides depending on checkbox toggle up in jQuery
					foreach ($connection_pages as $connection) {
						$connection_type = get_the_title($connection);
						$connection_tab_link1 = strtolower(str_replace(" ", "-", $connection_type));
						$connection_tab_link = preg_replace("/[^a-zA-Z0-9]+/", "", $connection_tab_link1);
						$businessPlanClass = '';
						
					}

					// Checks the URL and highlights the relevant tab (ie, if it's MBE tab it checks the business plans box and highlights the first MBE tab)
					switch (true){
						case stristr($last,'internet'): ?>
							<script type="text/javascript">
							$(window).on('load',function(){
								$('.card-tabs-bar a:first-child').click();
								$('.card-tabs-bar a:nth-last-child(-n+3)').hide();
								$('.plan-preloader').remove();
								$('.tabs').css('opacity','1');
								$('.tabs').css('height','auto');
								
							});
							</script>
							<?php
							break;
						case stristr($last,'business-nbn'): ?>
							<script type="text/javascript">
							$(window).on('load',function(){
								$('.card-tabs-bar a').hide();
								$('.business-checkbox').hide();
								$('.card-tabs-bar a:last-child').click();
								$('.plan-preloader').remove();
								$('.tabs').css('opacity','1');
								$('.tabs').css('height','auto');
						  	});
							</script>
							<?php
						break;
							case stristr($last,'business'): ?>
							<script type="text/javascript">
							$(window).on('load',function(){
								$('.card-tabs-bar a:nth-child(-n+2)').hide();
								$('.business-checkbox').hide();
								$('.card-tabs-bar a:last-child').click();
								$('.plan-preloader').remove();
								$('.tabs').css('opacity','1');
								$('.tabs').css('height','auto');
						  	});
							</script>
							<?php
						break;
						case stristr($last,'ethernet'): ?>
							<script type="text/javascript">
							$(window).on('load',function(){
								$('.card-tabs-bar a:nth-child(-n+2)').hide();
								$('.card-tabs-bar a:last-child()').hide();
								$('.business-checkbox').hide();
								$('.card-tabs-bar a:nth-child(3)').click();
								$('.plan-preloader').remove();
								$('.tabs').css('opacity','1');
								$('.tabs').css('height','auto');
						  	});
							</script>
						<?php
						break;
							case stristr($last,'nbn'): ?>
							<script type="text/javascript">
							$(window).on('load',function(){
								$('.card-tabs-bar a').hide();
								$('.business-checkbox').hide();
								$('.card-tabs-bar a:first-child').click();
								$('.plan-preloader').remove();
								$('.tabs').css('opacity','1');
								$('.tabs').css('height','auto');
						  	});
							</script>
						<?php
						break;
							break;
							case stristr($last,'adsl2'): ?>
							<script type="text/javascript">
							$(window).on('load',function(){
								$('#tab-adsl2 .pricing .pricing-price').remove();
								$('.card-tabs-bar a').hide();
								$('.business-checkbox').hide();
								$('.card-tabs-bar a:nth-child(2)').click();
								$('.plan-preloader').remove();
								$('.tabs').css('opacity','1');
								$('.tabs').css('height','auto');
						  	});
							</script>
						<?php
						break;
					} ?>
			<a id="pricingTabs"></a>
				<div class="tabs" <?php if (stristr($last,"ethernet") == false){?>style="opacity:0;height:0;"<?php } ?>>
				
						<?php
						//Loop to generate the actual tab frames
						foreach ($connection_pages as $connection) {
						$connection_type = get_the_title($connection);
						$cis_standalone = get_field("cis_standalone",$connection);
						$cis_bundled = get_field("cis_bundled",$connection);
						$connection_tab_link1 = strtolower(str_replace(" ", "-", $connection_type));
						$connection_tab_link = preg_replace("/[^a-zA-Z0-9]+/", "", $connection_tab_link1);
						?><div id="tab-<?php echo ($connection_tab_link);?>" data-tab="<?php echo ($connection_type);?>">

						<div class="row">
							<div class="pricing bottommargin clearfix">
								<?php

								$plan_query = new WP_Query();
								$args1 = array(
									'post_type' => 'page',
									'post_parent' => $connection,
									'ignore_sticky_posts' => true,
									'fields' => 'ids',
									'order' =>'DESC',
									'orderby' => 'date'
								);
								$plan_pages = $plan_query->query($args1);
								//inner loop inside tab frame to fetch plan data and generate pricing boxes for each
								foreach ($plan_pages as $plan) {
									$plan_id = get_the_id($plan);
									$plan_title =  get_the_title($plan);
									$plan_monthly_cost  = number_format(get_field("monthly_cost", $plan),2);
									$plan_payg_penalty  = number_format(get_field("payg_cost", $plan),2);
									$six_wire_price  = number_format(get_field("six_wire_price", $plan),2);
									$eight_wire_price  = number_format(get_field("eight_wire_price", $plan),2);
									$plan_payg_cost = number_format($plan_payg_penalty + $plan_monthly_cost, 2);
									//these are hardcoded because you'd need to update every single ACF field which isn't tennable
									$plan_setup_cost = 199.00;
									$plan_setup_cost_12 = 60.50;
									$plan_setup_cost_24 =  0.00;
									$plan_setup_cost_36 =  0.00;
									$plan_terms = get_field("terms", $plan);
									$plan_data = get_field("data", $plan);
									$plan_min_upload = get_field("min_upload_speed", $plan);
									$plan_min_download = get_field("min_download_speed", $plan);
									$plan_max_upload = get_field("max_upload_speed", $plan);
									$plan_max_download = get_field("max_download_speed", $plan);
									$plan_comingsoon = get_field("coming_soon", $plan);

									?>
								<div class="col-md-<?php if (stristr($plan_title,'NBN')){echo ('3');}else{echo ('4');} ?>">
									<div class="pricing-box <?php echo ($connection_tab_link);
									 if ($plan_comingsoon == true){echo ' coming-soon'; } ?>">
										<div class="pricing-title">
											<h3><?php echo($plan_title);?></h3>
										</div>
										<div class="pricing-price">
											<span><strong><?php if (empty($plan_min_download)==false ){
													 echo($plan_min_download);?>/<?php echo($plan_min_upload) ?> - <?php ;
												 }
												 echo($plan_max_download);?>/<?php echo($plan_max_upload);?> </strong><sub>Mbps<sup>*</sup></sub>
												 <span class="downloadupload"> download/upload*</span>
												 
											
											<!--<span class="price-unit">&dollar;</span>
											<span class="pricing-number">< ?php echo($plan_monthly_cost);?></span><span class="price-tenure">/mo</span>-->
										</div>
										<div class="pricing-mincost">
											<span><?php echo($plan_data);?> DATA</span>
										</div>
										<div class="pricing-features">
											<ul>
												<li class="termSelector"><span><strong>Select a term:</strong> </span><br/>

													<select class="sm-form-control term" name="term" id="term" <?php if ($plan_comingsoon == true){ ?>disabled="disabled"<?php }?>>			

													<?php foreach (array_reverse($plan_terms) as $term){?>
														<?php if($term =="1"){?>
															<option value="<?php echo $term ?>" data-monthly-cost="<?php echo($plan_payg_cost);?>" data-total-cost="<?php echo(number_format($plan_setup_cost + $plan_payg_cost));?>"  data-setup-cost="<?php echo $plan_setup_cost;?>"><?php echo ($term . " month");
															
														}else if($term =="12"){ ?>
															<option value="<?php echo $term ?>" data-monthly-cost="<?php echo($plan_monthly_cost);?>" data-total-cost="<?php echo(number_format($plan_setup_cost_12 + ($plan_monthly_cost * $term)));?>" data-setup-cost="<?php echo $plan_setup_cost_12;?>"><?php echo ($term . " months");
														}else if($term =="24"){ ?>
															<option value="<?php echo $term ?>" data-monthly-cost="<?php echo($plan_monthly_cost);?>" data-total-cost="<?php echo(number_format($plan_setup_cost_24 + ($plan_monthly_cost * $term)));?>"  data-setup-cost="<?php echo $plan_setup_cost_24;?>" selected><?php echo ($term . " months");
														}else if($term =="36"){ ?>
															<option value="<?php echo $term ?>" data-monthly-cost="<?php echo($plan_monthly_cost);?>" data-total-cost="<?php echo(number_format($plan_setup_cost_36 + ($plan_monthly_cost * $term)));?>"  data-setup-cost="<?php echo $plan_setup_cost_36;?>"><?php echo ($term . " months");
														}	
														?></option>
													<?php }?>
													</select>
												</li>
												<?php  if ((stristr($connection_type,"efm") == true) || (stristr($connection_type,"mbe") == true)){?>
												<li class="wireSelector">
													<span><strong>More wires:</strong> </span><br/>
													<select class="sm-form-control wire" name="wire" id="wire">
														<option selected value="<?php echo $plan_monthly_cost;?>" data-total-cost="<?php echo(number_format($plan_setup_cost_36 + ($plan_monthly_cost * $term),2));?>">Four Wires: $<?php echo $plan_monthly_cost;?></option>
														<option value="<?php echo $six_wire_price;?>" data-total-cost="<?php echo(number_format($plan_setup_cost_36 + ($six_wire_price * $term),2));?>">Six Wires: from $<?php echo $six_wire_price;?> per month</option>
														<option value="<?php echo $eight_wire_price;?>" data-total-cost="<?php echo(number_format($plan_setup_cost_36 + ($eight_wire_price * $term),2));?>">Eight Wires: from $<?php echo $eight_wire_price;?> per month</option>
													
													</select>
												</li>
												<?php } ?>
												<li class="monthlycost"><?php if ((strpos($connection_type, 'EFM')!==false ) || (strpos($connection_type, 'MBE')!==false )){
																	?>Starting from
																<?php }?><strong> &dollar;<?php echo($plan_monthly_cost);?></strong> per month<?php  if (stristr($connection_type,"business") == true){?><sup>&#8225;</sup><?php } ?></li>
												<li class="setupcost"><strong>Minimum setup cost:</strong> <span>$<?php echo $plan_setup_cost_24;?>

												
												
												</span><sup>&#8224;</sup><?php  if (stristr($connection_type,"business") == true){?><sup>&#8225;</sup><?php } ?></li>
												<!-- Multiplied by 24 by default as it's the default term, but will change via JS on select of new term -->
												<li class="totalcost"><strong>Total minimum cost:</strong> <span>$<?php echo(number_format( $plan_setup_cost + ($plan_monthly_cost * 24),2));?></span><sup>&#8224;</sup><?php  if (stristr($connection_type,"business") == true){?><sup>&#8225;</sup><?php } ?></li>
												<?php  if ((stristr($connection_type,"business")) || (stristr($connection_type,"efm")) || (stristr($connection_type,"mbe"))){?>
												<li><strong>5/1</strong> Contention ratio</li>
												<li><strong>Static</strong> IP</li>
												 <?php } ?>
												
												<li class="cislink">
												<?php if (empty($cis_standalone)==false ){ ?>  
													<a href="<?php echo($cis_standalone['url']);?>">Critical Information Summary (standalone) (PDF)</a>
													<?php ;
												} ?>
												<?php if (empty($cis_bundled)==false ){ ?>  
													<a href="<?php echo($cis_bundled['url']);?>">Critical Information Summary (bundled) (PDF)</a>
													<?php ;
												} ?>
												</li>
											</ul>
										</div>
										<div class="pricing-action">
											<a href="#planSignup" class="plan-select button button-red scroll <?php echo ($connection_tab_link);?>" data-plantitle="<?php echo $plan_title; ?>" data-planid="<?php echo $plan; ?>" data-plantype="<?php echo($connection_type . " " . $plan_data. " " . $plan_max_upload);?>">Select plan</a>
										</div>
										<?php if ($plan_comingsoon == true){ ?>
											<span class="comingsoon">Coming soon</span>
										<?php }?>
										
										
									</div>
								</div>
								<?php }?>
								<div class="col_full topmargin-sm clearfix disclaimer">
									<?php echo apply_filters('the_content', get_post_field('post_content', $connection));?>					
									
								</div>
							</div>
						</div>
					</div>
					<?php }?>

					<div id="planSignup" class="row">
						<div class="planSignup-inner">
						<h3 class="center">2. Reserve your plan</h3>
						<div class="alert-light alert nomargin clearfix">
							<div class="col_half nobottommargin">
								<div class="pricing nobottommargin clearfix">
									<div class="col-full notopmargin">
										<div class="heading-block center">
											<h3>Your selected plan:</h3>
										</div>

										 <div class="pricing-box nomargin">
										 <!-- Will fill with details on click of plan from Select Plan button via jQuery-->

										 </div>
									</div>
								</div>
							</div>
							<div class="col_half col_last nobottommargin notopmargin modal-padding clearfix">
								<div id="signup-step1">
									<h4 class="center">Enter your details</h4>
									<form id="planSelector" action="#" method="post" class="nobottommargin">
										<div class="form-process"></div>
										<div class="clearfix">
											<input type="text" class="sm-form-control required" id="name-field" name="name" value="" placeholder="Name*" required>
										</div>
										<div class="clearfix">
											<input type="text" class="sm-form-control" id="business-field" name="business" value="" placeholder="Business name">
										</div>

										<div class="clearfix">
											<input type="email" class="sm-form-control required" id="email-field" name="email" value="" placeholder="Email*" required>
										</div>

										<div class="clearfix">
											<input type="phone" class="sm-form-control" id="phone-field" name="phone" value="" placeholder="Phone*" required>
										</div>
										<div class="clearfix">
											<select class="sm-form-control" id="time" name="time">
												<option disabled selected>Best contact time</option>
												<option name="Any time" value="none">Any time</option>
												<option name="Morning" value="morning">Morning</option>
												<option name="Afternoon" value="afternoon">Afternoon</option>
												<option name="Evening" value="evening">Evening</option>
											</select>
										</div>
										<div class="clearfix alert alert-voip">
										  <input type="checkbox" class="sm-form-control" id="voip-home" name="meshproduct[]" value="VoIP Home"><label for="voip-home">YES, add Home VoIP for +$10 per month <i class="icon-phone3 icon-lg"></i></label>
										</div>
										<div class="clearfix alert alert-voip">
										  <input type="checkbox" class="sm-form-control" id="voip-mobile" name="meshproduct[]" value="VoIP Mobile"><label for="voip-mobile">YES, add Mobile VoIP for +$10 per month <i class="icon-phone icon-lg"></i></label>
										</div>
										<div class="clearfix alert alert-voip bottommargin-sm ">
											<input type="checkbox" class="sm-form-control" id="modem" name="meshproduct[]" value="modem"><label for="modem">I need a modem for $115 delivered</label>
										</div>
										<input type="hidden" id="planid" name="planid" value=""  />
										<input type="hidden" id="termchosen" name="termchosen" value=""  />
										<input type="hidden" id="address-field" name="address" value=""/>
										<div class="col_full" id="form-condition-submit">
										  <button type="submit" class="submit button nomargin button-large pull-right button-red">RESERVE NOW, PAY LATER</button>
										</div>
									</form>
								</div>
								<div id="signup-step2" class="hidden">
									<h4>Thanks for your enquiry</h4>

									<!--p>A confirmation has been sent to <span id="emailAddress">your email address</span>.</p-->
									<p>A consultant will be in touch shortly to discuss your account. Problems? Call us on <a href="tel:1300 080 820">1300 080 820</a>.</p>
								</div>
							</div>
						</div>
						
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
