<!-- Footer
		============================================= -->
		<footer id="footer" class="notopborder dark">
			<div class="container nopadding nomargin col_full nooverflow">

				<!-- Footer Widgets 
				============================================= -->
				<div class="footer-widgets-wrap clearfix">
					<div class="row clearfix">
						<div class="container">
							<div class="widget clearfix">
								<div class="row">
									<div class="col-xs-9">
										<div class="row bottommargin-sm">
											<div class="col_half">
												<img class="logo" src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/logo-dark.png" alt="MeshTelco" width="300"/>
											</div>
											<div class="col_half col_last notopmargin">
												<ul class="clients-grid grid-2 nobottommargin clearfix">
													<li><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/award-footer-white.png" alt="2017 ConnectHack Award Winner"/></li>
													<li class="australian-logo"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/australia-logo.png" alt="2017 Proudly Australian Owned and Operated"/><span>Proudly 100% Australian Owned &amp; Operated</span></li>
												</ul>
											
												
											</div>
										</div>
										<div class="row topmargin-lg">
											<div class="col-xs-4">
												<div class="widget clearfix contactDetails">
													<h3>Get in touch</h3>
													<?php
													$contactpageID = 51;
													$email = get_field("email", $contactpageID );
													$phone = get_field("phone", $contactpageID );
													$address = get_field("address", $contactpageID );
													?>
													<i class="icon-email3"></i> <a href="mailto:<?php echo $email;?>"><?php echo $email;?></a><br/>
													<i class="icon-call"></i> <span><?php echo $phone;?></span><br/>
													<i class="icon-location-arrow"></i> <span><?php echo $address; ?></span>
												</div>
												<div class="clearfix" data-class-xs="" data-class-xxs="">
													<a href="https://www.facebook.com/MeshTelco/" class="social-icon si-large si-borderless si-facebook" target="_blank">
														<i class="icon-facebook"></i>
														<i class="icon-facebook"></i>
													</a>

													<a href="https://twitter.com/mesh_telco" class="social-icon si-large si-borderless si-twitter" target="_blank">
														<i class="icon-twitter"></i>
														<i class="icon-twitter"></i>
													</a>

													<a href="https://plus.google.com/107272388782829256911" class="social-icon  si-large si-borderless si-google" target="_blank">
														<i class="icon-google-plus"></i>
														<i class="icon-google-plus"></i>
													</a>
													<a href="https://www.linkedin.com/company/meshtelco" class="social-icon  si-large si-borderless si-linkedin" target="_blank">
														<i class="icon-linkedin"></i>
														<i class="icon-linkedin"></i>
													</a>
												</div>
											</div>

											<div class="col-xs-4 widget_links">
												<h3>Navigate</h3>


											   <?php  wp_nav_menu( array( 
												'canvas' => 'top-header',
												'depth' => 1 
												)); ?>
											</div>
											<div class="col-xs-4 col_last">
												<h3>Our Sister Products</h3>
												<ul class="clients-grid grid-1 nobottommargin clearfix">

													<li><a href="https://lifewireless.com.au/" target="_blank"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/logo-lifewireless-white.png" alt="Life Wireless Fixed Wireless internet"></a></li>
													<li><a href="http://meshdigital.com.au/" target="_blank"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/logo-meshdigital-white.png" alt="MeshDigital Digital Signage"></a></li>
													<li><a href="http://www.meshone.com.au/" target="_blank"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/logo-meshone-white.png" alt="MeshOne Mesh Wireless Networks"></a></li>

												</ul>
											</div>
										</div>
									</div>
                                    <div class="col-xs-3">
                                       <!-- Facebook widget -->
                                        <div class="fb-page" data-href="https://www.facebook.com/MeshTelco/" data-tabs="timeline" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/MeshTelco/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/MeshTelco/">MeshTelco</a></blockquote></div>
                                    </div>
                                 </div>
                              </div>
                       		</div>
						</div>
					</div>
           <?php wp_footer(); ?>
            <div class="row clearfix">
                    <!-- Copyrights
			============================================= -->
				<div id="copyrights">
                    <div class="container clearfix center">
    
                       <span> &copy; <?php echo date('Y'); ?> All Rights Reserved by MeshTelco.</span> <a href="<?php echo site_url(); ?>/legal/">Legal</a>
    
                    </div>
                </div><!-- #copyrights end -->
            </div>
   		</div><!-- .footer-widgets-wrap end -->
	</footer><!-- #footer end -->
	<script type="text/javascript">
		var loaded = false; 
		$(document).ready(function(){
			setTimeout(function(){ 
				var loaded = true; 
				//console.log("loaded: " + loaded);
				//Preloader, checks whether ga variable for Analytics is set before removing preloader as this loads last.
				 if( typeof loaded !== 'undefined'){
					 $('#wrapper').animate({opacity:1}, 500);
					$('.loader-holder').fadeOut();
				 }
			}, 1500);
		});
	</script>