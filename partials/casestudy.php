<section id="casestudy" class="slider-container nomargin dark">
	<div class="row">
		<ul class="slider slider-list nothovered">
		
			<li>
				<div class="container clearfix notopmargin">
					<div class="row clearfix nomargin">
						<div class="heading-block center">
							<h2>Case study: Monash Council</h2>
						</div>
						<div class="col_full heading-block nobottommargin">
							<ul>
								<li>Proof of concept to Monash Council for multi hop technology</li>
								<li>Multi-hop capability for 4K camera and data transfers via the air</li>
								<li>Live presentation and deployments with temporary structure and temporary power availability to showcase mobility, possible variety and methods of deployments.</li>
							</ul>
						</div>
						
					</div>
				</div>
			</li>
			
			<li>
				<div class="container clearfix notopmargin">
					<div class="row clearfix nomargin">
						<div class="heading-block center">
							<h2>Case study: Anytime Fitness Ferntree Gully</h2>
						</div>
						<div class="col_full heading-block nobottommargin">
							<ul>
								<li>Business grade point-to-point link - 10km</li>
								<li>Dedicated fixed-IP requirement</li>
								<li>Once of solution for legacy ADSL issues that constantly drops and causes alarm issues</li>
								<li>All cloud based applications</li>
								<li>IP based CCTV monitoring and alarm system requirements</li>
								<li>Immediate deployments, no "NBN" wait times</li>
							</ul>
						</div>
					</div>
				</div>
				
			</li>
			<li>
				<div class="container clearfix notopmargin">
					<div class="row clearfix nomargin">
						<div class="heading-block center">
							<h2>Case study: Park Avenue Accommodation Group</h2>
						</div>
						<div class="col_full heading-block nobottommargin">
							<ul>
								<li>Full WiFi system deployment with splash page access for hotel guest</li>
								<li>Providing both design, consultancy, deployment strategy and support for hotels &amp; accommodations group</li>
								<li>Extension of on premise network to remote office or building via LifeWireless</li>
								<li>Full integration of 2 buildings into a single network</li>
							</ul>
						</div>
					</div>
				</div>
			</li>
		</ul>
	</div>
</section>
<div class="section dark cta-green notopmargin nobottomborder">
	<div class="container clearfix nobottomborder">
		<div class="column col_half nobottommargin">
			<div id="section-buy" class="heading-block nobottomborder page-section center topmargin-sm">
				<h2>Ready for better internet?</h2>
			 </div>
		</div>
		<div class="center col_half col_last nobottommargin">
			<a href="#contact" class="scroll button button-white button-xlarge button-fullwidth">GET IN TOUCH <i class="icon-caret-down"></i></a>
		</div>
	</div>
</div>

            