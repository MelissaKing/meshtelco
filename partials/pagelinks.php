<!-- locations section -->

<?php 
$link_icon_1 = get_field("link_icon_1", $pageID);
$link_link_1 = get_field("link_link_1", $pageID );
$link_title_1 = get_field("link_title_1", $pageID );
$link_text_1 = get_field("link_text_1", $pageID );
$link_button_text_1 = get_field("link_button_text_1", $pageID );

$link_icon_2 = get_field("link_icon_2", $pageID);
$link_link_2 = get_field("link_link_2", $pageID );
$link_title_2 = get_field("link_title_2", $pageID );
$link_text_2 = get_field("link_text_2", $pageID );
$link_button_text_2 = get_field("link_button_text_2", $pageID );

$link_icon_3 = get_field("link_icon_3", $pageID);
$link_link_3 = get_field("link_link_3", $pageID );
$link_title_3 = get_field("link_title_3", $pageID );
$link_text_3 = get_field("link_text_3", $pageID );
$link_button_text_3 = get_field("link_button_text_3", $pageID );

$link_icon_4 = get_field("link_icon_4", $pageID);
$link_link_4 = get_field("link_link_4", $pageID );
$link_title_4 = get_field("link_title_4", $pageID );
$link_text_4 = get_field("link_text_4", $pageID );
$link_button_text_4 = get_field("link_button_text_4", $pageID );

 if (empty($link_text_4) == false){
	 $colwidth="one_fourth";
 } else if(empty($link_text_3) == false){
	 $colwidth="one_third";
 }else{
	 $colwidth="half";
 }

if (empty($link_text_1)==false){?>
<section id="pageLinks" class="section noborder notopmargin">
	<div class="container clearfix nobottommargin">
		<div class="row clearfix notopmargin icons">
			
			<div class="col_<?php echo $colwidth; ?> nobottommargin">
				<div class="feature-box fbox-center fbox-effect">
					<a href="<?php echo $link_link_1;?>" class="fbox-icon">
						<i class="icon-<?php echo $link_icon_1;?>"></i>
						<h3><?php echo $link_title_1;?></h3>
					</a>

					<p><?php echo $link_text_1;?></p>
					<a class="button button-red button-fullwidth button-large" href="<?php echo $link_link_1;?>"><?php echo $link_button_text_1;?></a>
				</div>
			</div>
			

			<div class="col_<?php echo $colwidth; ?>  <?php  if (empty($link_text_3) == true){ echo 'col_last'; } ?> nobottommargin">
				<div class="feature-box fbox-center fbox-effect">
					<a href="<?php echo $link_link_2;?>" class="fbox-icon">
						<i class="icon-<?php echo $link_icon_2;?>"></i>
						<h3><?php echo $link_title_2;?></h3>
					</a>
					<p><?php echo $link_text_2;?></p>
					<a class="button button-red button-fullwidth button-large" href="<?php echo $link_link_2;?>"><?php echo $link_button_text_2;?></a>
				</div>
			</div>
			
			<?php 
			//Checks if Icon 3 is set, and if so makes the fourth icon box
			if (empty($link_text_3) == false){ ?>
			<div class="col_<?php echo $colwidth; ?> <?php  if (empty($link_text_4) == true){ echo 'col_last'; } ?> nobottommargin">
				<div class="feature-box fbox-center fbox-effect">
					<a href="<?php echo $link_link_3;?>" class="fbox-icon">
						<i class="icon-<?php echo $link_icon_3;?>"></i>
						<h3><?php echo $link_title_3;?></h3>
					</a>
					<p><?php echo $link_text_3;?></p>
				<a class="button button-red button-fullwidth button-large" href="<?php echo $link_link_3;?>"><?php echo $link_button_text_3;?></a>
				</div>
			</div>
			<?php } 
			//Checks if Icon 4 is set, and if so makes the fourth icon box
			if (empty($link_text_4) == false){ ?>
			<div class="col_<?php echo $colwidth; ?> col_last nobottommargin">
				<div class="feature-box fbox-center fbox-effect">
					<a href="<?php echo $link_link_4;?>" class="fbox-icon">
						<i class="icon-<?php echo $link_icon_4;?>"></i>
						<h3><?php echo $link_title_4;?></h3>
					</a>
					<p><?php echo $link_text_4;?></p>
					<a class="button button-red button-fullwidth button-large" href="<?php echo $link_link_4;?>"><?php echo $link_button_text_4;?></a>
				</div>
			</div>
			<?php } ?>
		</div>
	</div>
</section>
<?php } ?>