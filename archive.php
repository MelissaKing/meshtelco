<?php /**
 * Template Name: Archive
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */?>

<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>
	<?php include 'header.php';?>

	<!-- Document Title
	============================================= -->
	<title><?php bloginfo( 'name' ); ?> | Blog Archive</title>

</head>

<body class="stretched blog" data-loader-color="#E30160">
	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TH927RC"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->
	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">

		<?php include 'menu.php';?>
        
        <section id="slider" class="slider-parallax hero-blog">
            
			<div class="container clearfix">
                <div class="heading-block nobottomborder col_half">
                   <?php
							$page = get_page_by_title('Blog title');
							$content = apply_filters('the_content', $page->post_content);
							echo $content;?>
                </div>
            </div>
		</section>
        

		<!-- Content
		============================================= -->
		<section id="content">
			<div class="nopadding content-wrap">
           
            
            <!-- intro -->
            <div class="section nopadding nomargin">
                    <div class="container clearfix nobottommargin topmargin-lg">
                        <div class="row clearfix nomargin">
                        	<div class="col_full">
                            	<!-- Posts
						============================================= -->
						<div id="posts">


                        <!-- Post Content
                        ============================================= -->
                        <div class="postcontent nobottommargin clearfix">
                        
                            <?php if ( have_posts() ) : ?>
    
                            <?php if ( is_home() && ! is_front_page() ) : ?>
                                <div class="entry-title">
                                    <h2><?php single_post_title(); ?></h2>
                                </div>
                            <?php endif; ?>
                
                            <?php
                            // Start the loop.
                            while ( have_posts() ) : the_post();
                
                                /*
                                 * Include the Post-Format-specific template for the content.
                                 * If you want to override this in a child theme, then include a file
                                 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
                                 */
                                get_template_part( 'content_archive', get_post_format() );
                
                            // End the loop.
                            endwhile;
                
                            
							// If no content, include the "No posts found" template.
							else :
								get_template_part( 'content_archive', 'none' );
					
							endif;
							?>
                            
                            <!-- Pagination
						============================================= -->
						<div class="clear"></div>
                            <div class="pager margintop-lg">
                            <?php // Previous/next page navigation.
                            the_posts_pagination( array(
                                'prev_text'          => __( '&laquo; Newer', 'Draftee' ),
                                'next_text'          => __( 'Older &raquo;', 'Draftee' ),
                                'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'twentyfifteen' ) . ' </span>',
                            ) );?>
                            </div><!-- .pager end -->
    
                        </div><!-- .postcontent end -->
                	</div>
                                <?php include 'right_sidebar.php';?>
                            </div>
                        </div>
                    </div>
                </div>
                
            

                 <?php include 'cta.php';?>
    
		</section><!-- #content end -->

		<?php include 'footer.php';?>

	</div><!-- #wrapper end -->
	<!-- Footer Scripts
	============================================= -->
	<script type="text/javascript" src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/functions.js"></script>

</body>
</html>